# Corpuce Marilyn

Un "corpuce" désigne :

> «&nbsp;un ensemble d’objets littéraires issus de pratiques d’écritures contemporaines s’inscrivant dans les espaces de publication non-institutionnels&nbsp;».  
> -- [Démarche et problématique de recherche](http://corpuces.net/chantiers/)

Le Corpuce Marilyn résulte de la collecte du projet Marilyn d'Anne Savelli. Ce répertoire met en oeuvre la collecte, l'analyse et l'éditorialisation d'un _corpus situé_.

Ce travail est en cours :

**Collecte:**

- [x] Série de tests préliminaires
    - [`test-minet.ipynb`](test-minet.ipynb) → notebook de test 
    - [`/test-minet/`](/test-minet/) → requetes et collectes correspondantes (csv)
- [x] Collecte sur Twitter (en cours)
    - [`corpuce-maker-marilyn.ipynb`](corpuce-maker-marilyn.ipynb) → notebook de collecte du corpuce Marilyn
    - [`/corpuce_marylin/`](/corpuce_marylin/) → corpuce Marilyn
- [ ] Collecte sur Instagram
- [ ] Collecte sur Facebook

**Analyse littéraire:**

- [x] Classification et éditorialisation de l'iconothèque Twitter (en cours)
    - [PixPlot iconothèque Twitter](https://corpuces.gitpages.huma-num.fr/projet-marilyn/visualisation-iconotheque/)
- [x] Analyse de réseaux (en cours)
    - exemple : [réseau des hashtags](https://corpuces.gitpages.huma-num.fr/projet-marilyn/analyse-reseau/all.twitwi__nt0_lt0_HTN.html), 
    - [sources (gitlab)](https://gitlab.huma-num.fr/corpuces/projet-marilyn/analyse-reseau/) et [autres réseaux](https://corpuces.gitpages.huma-num.fr/projet-marilyn/analyse-reseau/)
- [x] Typologie du collectif
    1. `/corpuce_marilyn/requete_mentionned_tag.csv` : extraction des profils mentionnés et catégorisation selon leur type (2023)
    2. `/corpuce_marilyn/requete_mentionned_tag-typologie.csv` : typologie des profils mentionnés par occurrences (2024)
- [ ] Analyse photolittéraire

**Communications:**

- Servanne Monjour et Nicolas Sauret. [«&nbsp;Archiver le web littéraire : Défis méthodologiques et conceptuels&nbsp;»](https://corpuces.gitpages.huma-num.fr/slides-respadon). Colloque ResPaDon : [_Le web : source et archive_](https://respadon.sciencesconf.org/), Université de Lille, 3-5 avril 2023
- Servanne Monjour. «&nbsp;#AlerteMarilyn: l'icono-graphie selon Anne Savelli&nbsp;». Colloque [«&nbsp;Les iconothèques d'écrivain·e·s contemporain·e·s (1980 - aujourd'hui)&nbsp;»](https://oic.uqam.ca/en/evenements/les-iconotheques-decrivain-e-s-contemporain-e-s-1980-aujourdhui), organisé par Anne Reverseau, Bertrand Gervais et Corentin Lahouste, UQAM, avril 2022 ([Vidéo](https://oic.uqam.ca/en/communications/alertemarilyn-licono-graphie-selon-anne-savelli) et [Communication](https://corpuces.gitpages.huma-num.fr/iconotheque/#/))

---

![CC-BY-SA 4.0](http://i.creativecommons.org/l/by-sa/3.0/80x15.png)
Ces travaux sont sous licence [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 
